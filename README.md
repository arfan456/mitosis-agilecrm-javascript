# README #

##Configuration##
* Clone the config.sample.js file and create you own config.js.
* Provide values to all configuration variable in the config file.
* Load the js files according to the following order,
          a). jquery.js
          b). jquery.cookie.js
          c).config.js
          d).MitosisLeadscore.js
          e). Agile CRM javascript library
* _agile.set_account('JAVASCRIPT KEY', 'AGILECRM DOMAIN NAME') use this function and set the account.
* Now you can use the library functions.

##Public Functions##

###1. addScore(catagory, score)###
Public Main function to add, update lead score in local/agile. 
####Param###
* catagory - [string] score catagory (ex: education).
* score - [int] lead score (ex: 2)
####Logic####
The function checks if the user has registered with the website by checking the AgileCRM cookie, if the user is registered the score shall be directly update to AgileCRM.

If the user has not registered yet, the user scores will be locally stored and updated anonimously.

###2. addScoreByUrl(urlPart, catagory, score) ###
Public Main function to add lead score based on url. This will add score if the current url contains a specified URL part.
#### Param ####
* urlPart - [string] url section (ex: if you want to add lead score if the current url contains 'campus', the url part would be 'campus').
* catagory - [string] score catagory (ex: education).
* score - [int] lead score (ex: 2).
####Logic####
The function checks if the current url contains the specified url part and if contains it adds lead score, based on specified catagory and score amount.
###3. pushAllLocalScoreToAgile(email) ###
Public main function to push all locally stored lead score values to AgileCRM.
####Param####
* emial - [string] user email which was captured upon registration with AgileCRM.
####Logic####
Pushes all locally stored lead score values to a specified contact in AgileCRM based on the contact email.

##Private Functions##

###1. updateScoreInAgileCrm(email, catagory, score)###
Private function to update lead score on AgileCRM for already registered users.
####Param####
* email - [string] AgileCRM contacts' email.
* catagory - [string] score catagory (ex: education).
* score - [int] lead score (ex: 2).
####Logic####
The function do ajax post to the specified ajax url in the config.js file to update lead score in AgileCRM specified contact.
The function posts the following variables to the ajax request.

* action - predefined as addLeadScore, no need to modiy.
* email - email which is passed from the function param.
* catagory - catagory which is passed from the function param.
* score -score which is passed from the function param.

###2. setScore(catagary, score, callback)###
Private function to process the locally stored score and update the latest scores on a local variable.
####param####
* catagory - [string] score catagory (ex: education) passed through the main function.
* score - [int] lead score (ex: 2) passed through the main function.
* callback - [function] callback function which is to be executed after this function execution.
####Logic####
The function checks if there is any score locally stored, if so then update the score with the latest score values, else add new score based on the category.

###3. getCookie(catagory, score ,callback) ###
Private function to read lead score from local cookie and assign to local variable.
####Param####
* catagory - [string] catagory name which will be passed to the callback function.
* score - [int] score which will be passed to the callback function.
* callback - [function] function which will be executed after this function execution.
####Logic####
The function reads the locally stored cookie and convert the cookie value to json format then the value is assign to a local variable for further usage.

###4. setInitialScore(catagory, score, callback)###
Private function to set the lead score locally for the first time user.
####Param####
* catagory - [string] Score catagory which is passed through the main function.
* score - [int] Score value which is passed through the main function.
* callback - [function] function which is to be executed after this function is executed.
####Logic####
This function takes the catagory and the score and stores in local variable which will be later stored in the local cookie.

###5. setCookie() ###
Private function, which will store the local score variable to a specified local cookie.

##Sample Usage##
```html
<!--Loading the required libraries-->
<script src="bower_components/jquery/dist/jquery.js"></script>
<script src="bower_components/jquery.cookie/jquery.cookie.js"></script>
<script src="dist/config.js"></script>
<script src="dist/MitosisLeadScore.js"></script>

<!--Loading the AgileCRM js Library and set the account-->
<script type="text/javascript" src="https://arfan.agilecrm.com/stats/min/agile-min.js"> </script>
<script type="text/javascript" >
    _agile.set_account('javascript api key', 'agile domain name');
    _agile.set_tracking_domain ('abc.my');
</script>

<!--After the libraries are loaded now you can use the main functions to add, update score-->

<!--Add Score 3 to education catagory for an anonymous user-->
<script>
    addScore('education', 3);
</script>

<!--Add Score 2 to scholarship catagory for an anonymous user if the user visited url contains 'finance' key word-->
<script>
    addScoreByUrl('finance', 'scholarship', 2);
</script>

<!--Once the user has registered with AgileCRM and the contact has being successfully created push all locally stored score to AgileCRM-->
<!--registered user email which is grabbed upon user registration-->
<script>
    pushAllLocalScoreToAgile('arfan@mitosis.my');
</script>

```