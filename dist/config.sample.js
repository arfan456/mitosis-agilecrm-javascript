/**
 *  General Settings.
 */

/**
 * To enable debug mode set the variable to 'true'.
 * @type {boolean}
 */
var DEBUGMODE=true;

/**
 * Agile CRM Javascript API key.
 * @type {string}
 */
var AGILEJAVASCRIPTAPIKEY="8ikfrjd0kn2c9qg4i8u48pv3t1";

/**
 * Local Cookie Name.
 * @type {string}
 */
var LOCALCOOKIENAME="mitosisAgileLeadScore";

/**
 * Cookie Domain Name.
 * @type {string}
 */
var DOMAINNAME="localhost";


/**
 * URL of the Motosis Agile lead score API
 * @type {string}
 */
var AJAXURL='http://192.168.1.2/AgileModule/index.php';


if(DEBUGMODE){
    console.log('config loaded');
}


