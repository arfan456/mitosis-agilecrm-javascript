/**
 * ___Usage Guide___
 *
 * 1. Clone the config.sample.js file and create you own config.js.
 * 2. Provide values to all configuration variable in the config file.
 * 3. Load the js files according to the following order,
 *          a). jquery.js
 *          b). jquery.cookie.js
 *          c).config.js
 *          d).MitosisLeadscore.js
 *          e). Agile CRM javascript library
 * 4.  _agile.set_account('JAVASCRIPT KEY', 'AGILECRM DOMAIN NAME') use this function and set the account.
 * 5.Now you can use the library functions.
 *
 *
 */

/**
 * Object where all the scores will be stored.
 * @type {{}}
 */
var cookieArray={};

/**
 * AgileCRM cookie name which contains the user email.
 * @type {string}
 */
var AGILEEMAILCOOKIENAME=AGILEJAVASCRIPTAPIKEY+"-agile-email";



/**
 * Main function to add, update lead score in local/agile.
 * Logic: the function checks if the user has registered with the website by checking the
 *        AgileCRM cookie, if the user is registered the score shall be directly update to
 *        AgileCRM.
 *        If the user has not registered yet, the user scores will be locally stored and
 *        updated anonimously.
 * @param catagory
 * @param score
 */
function addScore(catagory, score){
    if(DEBUGMODE){
        console.log(catagory);
    }

    if ($.cookie(AGILEEMAILCOOKIENAME)) {
        updateScoreInAgileCrm($.cookie(AGILEEMAILCOOKIENAME), catagory, score);
    }
    else if($.cookie(LOCALCOOKIENAME)){

        getCookie(catagory, score, function (catagory, score) {
            setScore(catagory, score, function () {
                    setCookie();
            });
        })

    }else{
        setInitialScore(catagory, score, function(){
            setCookie();
        });
    }
}
/**
 * Main function to add lead score based on url, this will add score if the current url contains,
 * the specified url part.
 *
 * @param urlPart
 * @param catagory
 * @param score
 */
function addScoreByUrl(urlPart, catagory, score){
    var curenturl = window.location.href;
    if (curenturl.toLowerCase().indexOf(urlPart) >= 0){
        if(DEBUGMODE){
            console.log(urlPart);
        }
        addScore(catagory, score);
    }
}

/**
 * Function to push all local scores to AgileCRM upon new user signup or registraion with,
 * the website (when new users email is captured for the first time use this function).
 *
 * @param email
 */
function pushAllLocalScoreToAgile(email){
    if(DEBUGMODE){
        console.log('ajax url '+AJAXURL);
    }

    if(AJAXURL) {

        cookieArray = JSON.parse($.cookie(LOCALCOOKIENAME));

        $.post( AJAXURL, { action: 'pushAllScore', email: email, data: JSON.stringify(cookieArray) } ).done(function(data){
            if(DEBUGMODE){
                console.log(data);
            }
        }).fail(function(data){
            if(DEBUGMODE){
                console.log(data);
            }
        })

    }else{
        console.log('Error, Please Define AJAXURL in config.js file');
    }

}



/**
 * Sub function to update the lead score directly in agile crm if the user has already registered,
 * with the site and the agile crm email cookie exists.
 *
 * @param email
 * @param catagory
 * @param score
 */
function updateScoreInAgileCrm(email, catagory, score){
    $.post( AJAXURL, { action: 'addLeadScore', email: email, catagory: catagory, score: score } ).done(function(data){
        if(DEBUGMODE){
            console.log(data);
        }
    }).fail(function(data){
        if(DEBUGMODE){
            console.log(data);
        }
    })
}

/**
 * Sub function to process the lead score, whcih includes updating existing catagory score,
 * or add new catagory with score.
 *
 * @param catagary
 * @param score
 * @param callback
 */
function setScore(catagary, score, callback) {

    if(typeof cookieArray[catagary] != 'undefined'){
        cookieArray[catagary]=parseInt(cookieArray[catagary]) + score;
    }else{
        cookieArray[catagary]=score;
    }
    callback();

}

/**
 * Sub function to read score from local cookie and store in local variable.
 *
 * @param catagory
 * @param score
 * @param callback
 */
function getCookie(catagory, score ,callback){
        var cookieVal=JSON.parse($.cookie(LOCALCOOKIENAME));
        if(DEBUGMODE){
            console.log('current cookie values');
            console.log(cookieVal);
        }

        cookieArray=cookieVal;
        callback(catagory, score);
}

/**
 * Sub function used if the score is going to be added for the first time in the,
 * local cookie.
 *
 * @param catagory
 * @param score
 * @param callback
 */
function setInitialScore(catagory, score, callback){
    cookieArray[catagory]=score;
    callback();
}

/**
 * Sub function to set the lead scores in local cookie.
 *
 */
function setCookie(){
    if(DEBUGMODE){
        console.log('Cookie domain name '+DOMAINNAME);
    }

    if(DOMAINNAME){
        $.cookie(LOCALCOOKIENAME,JSON.stringify(cookieArray),{path: '/', domain:DOMAINNAME})
    }else{
        $.cookie(LOCALCOOKIENAME,JSON.stringify(cookieArray),{path: '/'})
    }

}